//
//  PasswordTableViewCell.swift
//  MyPassword
//
//  Created by Un Vandy on 2/2/19.
//  Copyright © 2019 Un Vandy. All rights reserved.
//

import UIKit

class PasswordTableViewCell: UITableViewCell {

    @IBOutlet weak var passwordImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var usernameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(password: Password) {
        self.titleLabel.text = "Un Vandy"
        self.usernameLabel.text = "Testing Only"
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
