//
//  PasscodeViewController.swift
//  MyPassword
//
//  Created by Un Vandy on 2/10/19.
//  Copyright © 2019 Un Vandy. All rights reserved.
//

import UIKit

class PasscodeViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func loginButton(_ sender: Any) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let oldPassword = UserDefaults.standard.string(forKey: "Password1")
        //UserDefaults.standard.removeObject(forKey: "Password1")
        //print(oldPassword)
        if oldPassword == nil || oldPassword == "" {
            performSegue(withIdentifier: "newPasswordSegue", sender: nil)
        }
        self.passwordTextField.delegate = self
    }
    
    @IBAction func LoginClick(_ sender: Any) {
       
        
        let password = passwordTextField.text!
        let oldPassword = UserDefaults.standard.string(forKey: "Password1")
        
        if password != oldPassword { //Password incorrect
            showAlert(title: "Incorrect", message: "Password is not match ! Please try again !")
        }else { // password correct
            //performSegue(withIdentifier: "loginSegue", sender: nil)
        }
    }
 
    func checkEmpty(text: UITextField) -> Bool {
        if text.text! == "" {
            return false
        }
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        passwordTextField.becomeFirstResponder()
    }
    public func showAlert(title: String,message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    @objc func dismissKeyboard() {
        view.endEditing(false)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // For using 4 digi box
    /*
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (((textField.text?.count)!) < 1 ) && (string.count > 0) {
            
            if textField == textField1 {
                textField2.becomeFirstResponder()
            }
            if textField == textField2 {
                textField3.becomeFirstResponder()
            }
            if textField == textField3 {
                textField4.becomeFirstResponder()
            }
            if textField == textField4 {
                textField4.resignFirstResponder()
            }
            textField.text = string
            return false
        }else if (((textField.text?.count)!) >= 1) && (string.count == 0) {
            
            if textField == textField2 {
                textField1.becomeFirstResponder()
            }
            if textField == textField3 {
                textField2.becomeFirstResponder()
            }
            if textField == textField4 {
                textField3.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }else if ((textField.text?.count)!) >= 1 {
            textField.text = string
            return false
        }
        return true
    }
    */
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
