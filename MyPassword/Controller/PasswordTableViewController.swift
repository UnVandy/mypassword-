//
//  PasswordTableViewController.swift
//  MyPassword
//
//  Created by Un Vandy on 2/2/19.
//  Copyright © 2019 Un Vandy. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
@available(iOS 11.0, *)
class PasswordTableViewController: UITableViewController {
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var passwords: [Password]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchPassword()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let passwordData = passwords else {
            return 0
        }
        return passwordData.count
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let password = self.passwords?[indexPath.row]
        self.performSegue(withIdentifier: "addSegue", sender: password)
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "passwordCell", for: indexPath) as! PasswordTableViewCell

        // Configure the cell...
        let password = self.passwords?[indexPath.row]
        cell.titleLabel.text = password?.title
        cell.usernameLabel.text = password?.username
        return cell
    }
    /*
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            
            let alert = UIAlertController(title: "Confirm", message: "Are you sure want to Delele?",preferredStyle: UIAlertController.Style.alert)
                
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
                    self.dismiss(animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "Delete",
                                              style: UIAlertAction.Style.default,
                                              handler: {(_: UIAlertAction!) in
                                                //Delete
                                                let password = self.passwords?[indexPath.row]
                                                self.context.delete(password!)
                                                self.appDelegate.saveContext()
                                                self.fetchPassword()
                }))
                self.present(alert, animated: true, completion: nil)
            
           
        }
        let copy = UITableViewRowAction(style: .normal, title: "Copy") { (action, IndexPath) in
//            let password = self.passwords?[indexPath.row]
//            self.performSegue(withIdentifier: "addSegue", sender: password)
            UIPasteboard.general.string = self.passwords![indexPath.row].password
        }
        return [delete,copy]
    }
 */
    //for add swip to edit

    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let copyPass = UIContextualAction(style: .destructive, title: nil) { (action, view,_ ) in
            UIPasteboard.general.string = self.passwords![indexPath.row].password
            self.fetchPassword()
        }
        let copyUser = UIContextualAction(style: .normal, title: nil) { (action, view,_ ) in
            UIPasteboard.general.string = self.passwords![indexPath.row].username
            self.fetchPassword()
        }
        copyPass.image = UIImage(named: "unlock")
        copyUser.image = UIImage(named: "username")
        return UISwipeActionsConfiguration(actions: [copyUser,copyPass])
        
    }
    
    // swip to left to delete
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: nil) { (action, view, _) in
            
            let alert = UIAlertController(title: "Confirm", message: "Are you sure want to Delele?",preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
                self.dismiss(animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Delete",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            //Delete
                                            let password = self.passwords?[indexPath.row]
                                            self.context.delete(password!)
                                            self.appDelegate.saveContext()
                                            self.fetchPassword()
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        delete.image = UIImage(named: "trash")
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchPassword()
    }
    func fetchPassword() {
        self.passwords = try? context.fetch(Password.fetchRequest())
        self.passwords?.reverse()
        self.tableView.reloadData()
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addSegue" {
            let dest = segue.destination as? AddViewController
            dest?.passwords = (sender as! Password)
        }
    }

}
